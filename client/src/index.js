import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// import { Auth0Provider } from '@auth0/auth0-react';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <Auth0Provider
  //     domain="dev-kxxaffg8k3wi5scv.us.auth0.com"
  //     clientId="30MeD71Mci3hMJMqP2o6YPQn5HNBlAfx"
  //     audience="tdzTXaOETTNg3E-jFWbxIHI8Fhdy1TfedhWuPZzc8ExF5Sst4ufjjBMc4YxkZet4"
  //     authorizationParams={{
  //       redirect_uri: window.location.origin
  //     }}
  //   >
      <App />
    // </Auth0Provider>,
  );
reportWebVitals();
