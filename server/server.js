require('dotenv').config();

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to Database'));

app.use(express.json());

const allowedOrigins = [
    'http://carpooly.lk.s3-website-us-east-1.amazonaws.com',
    // Add more origins if needed
];

app.use(cors({
    origin: function (origin, callback) {
        // Allow requests with no origin (like mobile apps or curl requests)
        if (!origin) return callback(null, true);
        if (allowedOrigins.indexOf(origin) === -1) {
            const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }
}));

app.use(cors());

const ridesRouter = require('./routes/rides');
const profilesRouter = require('./routes/profiles');
const requestsRouter = require('./routes/requests');

app.use('/rides', ridesRouter);
app.use('/profiles', profilesRouter);
app.use('/requests', requestsRouter);

app.listen(5000, () => console.log('Server Started'));
